# Numa Data Movement Benchmark

## Build
```bash
mkdir build
cd build
cmake ..
make -j\`nproc\`
```

## Run
Use the `runall.py` helper script.
```bash
python runall.py build/benchmark-move-pages <out file path> <src_node> <node>
```
The script will output a csv in the path specified by <out file path>

## Analysis
The `analysis` directory contains a notebook used to generate the figures, as well as some results from A64FX, Kunpeng 920, Xeon Platinum 8180, Power9