//
// Created by lnghrdntcr on 6/7/23.
//
#include "Benchmark.h"

#if defined(X86_64)
#include <immintrin.h>
#elif defined(ARM_SVE)
#include <arm_sve.h>
#elif defined(ARM_NEON)
#include <arm_neon.h>
#endif

class StreamCopyBenchmark final: public Benchmark {

public:
  StreamCopyBenchmark(u32 src, u32 dest, u64 pages, u32 n_threads):
  Benchmark("stream-copy", src, dest, pages, n_threads) {}

  virtual void setup() override;
  virtual std::vector<BenchmarkResults> run(u32 times) override;
  virtual void destroy() override;

private:
  element_t *m_src {nullptr};
  element_t *m_dst {nullptr};
  u64 m_elems {};
  u64 m_bytes {0};
};


