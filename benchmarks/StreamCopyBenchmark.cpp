//
// Created by lnghrdntcr on 6/7/23.
//

#include "StreamCopyBenchmark.h"
#define UNROLL_FACTOR 8

void StreamCopyBenchmark::setup() {
  Timer<std::chrono::nanoseconds> timer("Setup");

  m_bytes = this->m_pages * this->m_page_size;

  if (m_src != nullptr) numa_free(m_src, m_bytes);
  if (m_dst != nullptr) numa_free(m_dst, m_bytes);

  m_elems = m_bytes / sizeof(element_t);
  m_src = (element_t *) numa_alloc_onnode(m_elems * sizeof(element_t), this->m_base_src);
  m_dst = (element_t *) numa_alloc_onnode(m_elems * sizeof(element_t), this->m_base_dest);
}

inline void _stream_copy_sve(element_t* __restrict dst, const element_t* __restrict src, const u64 elems) {

  u64 safe_elems = elems - 16;

#if defined(ARM_SVE)

  u64 i = 0;
  svbool_t pg = svwhilelt_b64(i, safe_elems);
  do
  {
    svuint32_t v = svld1_u32(pg, &src[i]);
    svst1_u32(pg, &dst[i], v);
    i += svcntd();
    pg = svwhilelt_b64(i, safe_elems);
  }
  while (svptest_any(svptrue_b64(), pg));

#endif

}

inline void _stream_copy_neon(element_t* __restrict dst, const element_t* __restrict src, const u64 elems) {

  static constexpr u32 stride = 128 >> 5;

#if defined(ARM_NEON)
  for(u64 i = 0; i < elems; i += stride) {

    auto* load_from  = &src[i];
    auto* store_into = &dst[i];

    uint32x4_t v = vld1q_u32(load_from);
    vst1q_u32(store_into, v);
  }
#endif
}

inline void _stream_copy_p9(element_t* __restrict dst, const element_t* __restrict src, const u64 elems) {

#if defined(P9)

    static constexpr u32 stride = 128 >> 5;
    for(u64 i = 0; i < elems; i += stride) {
      auto v  = *(__vector u32 *) &src[i];
      auto* store_into = (__vector u32 *) &dst[i];
      *store_into = v;
    }
#endif

  }


inline void _stream_copy_avx512(element_t* __restrict dst, const element_t* __restrict src, const u64 elems) {

#if defined(X86_64)
  static constexpr u32 stride = 512 >> 5;
  for(u64 i = 0; i < elems; i += stride) {

    auto* load_from  = &src[i];
    auto* store_into = &dst[i];

    auto v = _mm512_load_epi32(load_from);
    _mm512_store_epi32(store_into, v);

  }
#endif

}

inline void stream_copy(element_t* __restrict dst, const element_t* __restrict src, const u64 elems) {

  #if defined(X86_64)
  _stream_copy_avx512(dst, src, elems);
  #elif defined(ARM_SVE)
  _stream_copy_sve(dst, src, elems);
  #elif defined(ARM_NEON)
  _stream_copy_neon(dst, src, elems);
  #elif defined(P9)
  _stream_copy_p9(dst, src, elems);
  #else
  #pragma GCC ivdep
  #pragma GCC unroll 8
  for(u32 i = 0; i < elems; ++i){
      dst[i] = src[i];
    }

  #endif

}

std::vector<BenchmarkResults> StreamCopyBenchmark::run(fs::u32 times) {
  std::vector<BenchmarkResults> results;
  u32 elems_per_thread = (this->m_elems + this->m_n_threads - 1) / this->m_n_threads;
  const std::array<u32, 2> dests = {this->m_base_dest, this->m_base_src};
  for (u32 iters = 0; iters < times; ++iters) {
    for(const auto& dest: dests) {

      u64 elapsed = this->run_task_on_node([this, elems_per_thread](){
        #pragma omp parallel for
        for(u32 i = 0; i < this->m_n_threads; ++i) {
          u64 base_idx = i * elems_per_thread;
          element_t* base_dst_addr = &(this->m_dst[base_idx]);
          element_t* base_src_addr = &(this->m_src[base_idx]);
          stream_copy(base_dst_addr, base_src_addr, elems_per_thread);
        }
      }, "stream-copy", dest);

      std::cout << "[INFO] Size: " << (f64) (m_elems * sizeof(element_t) * B) / GB << "GB" << std::endl;
      std::cout << "[INFO] BW " << this->m_base_src << " > "
                << this->m_base_dest << ": "
                << (f64) (this->m_page_size * this->m_pages) / elapsed
                << " GB/s" << std::endl;



      std::swap(this->m_base_src, this->m_base_dest);
      this->setup();
      results.push_back({this->name(), this->m_base_src, this->m_base_dest, this->m_pages, this->m_page_size, elapsed});
    }
  }
  return results;

}

void StreamCopyBenchmark::destroy() {
  numa_free(m_src, this->m_page_size * this->m_pages);
  numa_free(m_dst, this->m_page_size * this->m_pages);
}