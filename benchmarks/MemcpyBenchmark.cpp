//
// Created by lnghrdntcr on 6/6/23.
//

#include "MemcpyBenchmark.h"

void MemcpyBenchmark::setup() {
  Timer<std::chrono::nanoseconds> timer("Setup");

  m_bytes = this->m_pages * this->m_page_size;

  if (m_src != nullptr) numa_free(m_src, m_bytes);
  if (m_dst != nullptr) numa_free(m_dst, m_bytes);

  m_elems = m_bytes / sizeof(element_t);
  m_src = (element_t *) numa_alloc_onnode(m_elems * sizeof(element_t), this->m_base_src);
  m_dst = (element_t *) numa_alloc_onnode(m_elems * sizeof(element_t), this->m_base_dest);
}

std::vector<BenchmarkResults> MemcpyBenchmark::run(fs::u32 times) {
  std::vector<BenchmarkResults> results;
  const std::array<u32, 2> dests = {this->m_base_dest, this->m_base_src};
  const u32 elems_per_thread = (this->m_elems + this->m_n_threads - 1) / this->m_n_threads;
  const u32 bytes_per_thread = elems_per_thread * sizeof(element_t);

  for (u32 iters = 0; iters < times; ++iters) {
    for(const auto& dest: dests) {

      u64 elapsed = this->run_task_on_node([this, bytes_per_thread, elems_per_thread](){
#pragma omp parallel for
        for(u32 i = 0; i < this->m_n_threads; ++i) {
          u64 base_idx = i * elems_per_thread;
          element_t* base_dst_addr = &(this->m_dst[base_idx]);
          element_t* base_src_addr = &(this->m_src[base_idx]);
          memcpy(base_dst_addr, base_src_addr, bytes_per_thread);
        }

      }, "memcpy", dest);

      std::cout << "[INFO] Size: " << (f64) (m_elems * sizeof(element_t) * B) / GB << "GB" << std::endl;
      std::cout << "[INFO] BW " << this->m_base_src << " > "
                << this->m_base_dest << ": "
                << (f64) (this->m_page_size * this->m_pages) / elapsed
                << " GB/s" << std::endl;



      std::swap(this->m_base_src, this->m_base_dest);
      this->setup();
      results.push_back({this->name(), this->m_base_src, this->m_base_dest, this->m_pages, this->m_page_size, elapsed});
    }
  }
  return results;

}

void MemcpyBenchmark::destroy() {
  numa_free(m_src, this->m_page_size * this->m_pages);
  numa_free(m_dst, this->m_page_size * this->m_pages);
}