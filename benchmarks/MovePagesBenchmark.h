//
// Created by lnghrdntcr on 6/6/23.
//
#pragma once
#include "Benchmark.h"



class MovePagesBenchmark final: public Benchmark {

public:
  MovePagesBenchmark(u32 src, u32 dest, u64 pages, u32 n_threads):
    Benchmark("move-pages", src, dest, pages, n_threads) {}

  virtual void setup() override;
  virtual std::vector<BenchmarkResults> run(u32 times) override;
  virtual void destroy() override;

private:
  std::vector<i32> m_pages_destinations;
  element_t * m_v {nullptr};
  void **m_page_addrs;
  i32 *m_status;
};




