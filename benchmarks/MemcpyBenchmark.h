//
// Created by lnghrdntcr on 6/6/23.
//
#pragma once

#include "Benchmark.h"

class MemcpyBenchmark final: public Benchmark {

public:
  MemcpyBenchmark(u32 src, u32 dest, u64 pages, u32 n_threads):
    Benchmark("memcpy", src, dest, pages, n_threads) {}

  virtual void setup() override;
  virtual std::vector<BenchmarkResults> run(u32 times) override;
  virtual void destroy() override;

private:
  element_t *m_src {nullptr};
  element_t *m_dst {nullptr};
  u64 m_elems {};
  u64 m_bytes {0};
};


