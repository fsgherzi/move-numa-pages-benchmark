//
// Created by lnghrdntcr on 6/6/23.
//

#include <thread>
#include "MovePagesBenchmark.h"

void MovePagesBenchmark::setup() {

  const u8 element_size = sizeof(element_t);

  const u64 elems = this->m_pages * this->m_page_size / element_size;
  const u64 elems_per_page = this->m_page_size / element_size;
  m_pages_destinations = std::vector<i32>(this->m_pages, this->m_base_src);


  std::cout << "[INFO] Moving " << elems << " elements" << std::endl;
  std::cout << "[INFO] Page amount: " << this->m_pages << "\nelements per page: " << elems_per_page << std::endl;
  std::cout << "[INFO] Memory footprint: " << ((f32)(this->m_pages * this->m_page_size * B) / GB) << " GB" << std::endl;

  m_v = new element_t[elems];


  // Initially, all uninitialized pages are owned by the kernel and are zero pages
  // trying to move it causes -EFAULT
  for (u32 i = 0; i < elems; ++i) {
    m_v[i] = 1; // To prevent the kernel from reporting that this is a zero page
  }

  m_page_addrs = new void *[this->m_pages];
  m_status = new i32[this->m_pages];

  {
    Timer<std::chrono::nanoseconds> timer("[INFO] get_page_addrs");
    for (u32 i = 0; i < this->m_pages; ++i) {
      u64 addr = reinterpret_cast<u64>(&m_v[i * elems_per_page]);
      u64 page_addr = addr & (~(this->m_page_size - 1));
      m_page_addrs[i] = reinterpret_cast<void *>(page_addr);
    }
  }

  // Get status
  if (move_pages(0, this->m_pages, m_page_addrs, nullptr, m_status, 0) == -1) {
    perror("[ERROR] get_page_status");
    exit(-1);
  }

  {
    Timer<std::chrono::nanoseconds> timer("[INFO] move_pages -> " + std::to_string(this->m_base_src));
    if ((move_pages(0, this->m_pages, m_page_addrs, m_pages_destinations.data(), m_status, MPOL_MF_MOVE)) == -1) {
      perror("[ERROR] move_pages");
      exit(-1);
    }
  }
}

std::vector<BenchmarkResults> MovePagesBenchmark::run(u32 times) {
  std::vector<BenchmarkResults> results;
  u32 dest_numa_node = this->m_base_dest;
  u32 src_numa_node = this->m_base_src;

  auto n_pages_per_thread = (this->m_pages + this->m_n_threads - 1) / this->m_n_threads;


  for(u32 i = 0; i < times; ++i) {

    for (auto destination: {dest_numa_node, src_numa_node}) {

      u32 benchmark_src = m_pages_destinations[0];
      u32 benchmark_dst = destination;

      std::fill(m_pages_destinations.begin(), m_pages_destinations.end(), destination);

      u64 elapsed = this->run_task_on_node([this, n_pages_per_thread](){

#pragma omp parallel for 
        for(u32 thread_id = 0; thread_id < this->m_n_threads; ++thread_id) {
          auto base_page_addr = &(m_page_addrs[thread_id * n_pages_per_thread]);
          auto base_dest_addr = &(m_pages_destinations.data()[thread_id * n_pages_per_thread]);
          auto base_status_addr = &(m_status[thread_id * n_pages_per_thread]);
          if ((move_pages(0, n_pages_per_thread, base_page_addr, base_dest_addr, base_status_addr, MPOL_MF_MOVE)) == -1) {
            perror("[ERROR] move_pages");
            exit(-1);
          }
        }

      }, "move-pages", destination);

      if (this->verify()) {
        for (u32 i = 0; i < this->m_pages; ++i) {
          int s = m_status[i];
          if (s == m_pages_destinations[0]) continue;
          if (s == -EACCES) {
            std::cout << " is mapped by multiple processes and can be moved if MPOL_MF_MOVE_ALL is specified" << std::endl;
          }

          if (s == -EBUSY) {
            std::cout << " is busy, try again later" << std::endl;
          }

          if (s == -EFAULT) {
            std::cout << " zero page or not mapped by the current process" << std::endl;
          }

          if (s == -EIO) {
            std::cout << " unable to be written back. Filesystem does not provide support for page swapping" << std::endl;
          }

          if (s == -EINVAL) {
            std::cout << " unable to be written back. Dirty page" << std::endl;
          }

          if (s == -ENOENT) {
            std::cout << " does not exist" << std::endl;
          }

          if (s == -ENOMEM) {
            std::cout << " unable to be allocated on target node" << std::endl;
          }
        }
      }

      std::cout << "[INFO] Throughput: " << (f64) this->m_pages / (f64) elapsed * 1e9 << " page/s\n";
      std::cout << "[INFO] Time to move one page: " << (f64) elapsed / (f64) this->m_pages / 1e3 << "us/page\n";
      std::cout << "[INFO] Bandwidth: " << (f64) (this->m_pages * this->m_page_size) / elapsed << "GB/s\n" << std::endl;
      results.push_back({this->name(), benchmark_src, benchmark_dst, this->m_pages, this->m_page_size, elapsed});
    }
  }
  return results;
}

void MovePagesBenchmark::destroy() {
  free(m_v);
  free(m_page_addrs);
  free(m_status);
}
