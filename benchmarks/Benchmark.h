//
// Created by lnghrdntcr on 6/6/23.
//
#pragma once

#include <vector>
#include <iostream>
#include <tuple>
#include <string>
#include <unistd.h>
#include <numaif.h>

#include <thread>
#include <array>
#include <numa.h>
#include <thread>

#include "../utils/fstypes.hpp"
#include "../utils/fsutils.hpp"
using namespace fs;

using element_t = u32;

struct BenchmarkResults {
  friend std::ostream &operator<<(std::ostream &os, const BenchmarkResults &results) {
    os << results.name << "," << results.numa_src << "," << results.numa_dst << "," << results.n_pages
       << "," << results.page_size << "," << results.time_ns;
    return os;
  }

  std::string name;
  u32 numa_src;
  u32 numa_dst;
  u64 n_pages;
  u64 page_size;
  u64 time_ns;
};

class Benchmark {
public:
  Benchmark(const std::string&& name, u32 src, u32 dest, u64 pages, u32 n_threads):
    m_name(name), m_base_src(src), m_base_dest(dest),
    m_pages(pages), m_page_size(sysconf(_SC_PAGESIZE)), m_n_threads(n_threads) {}

  Benchmark(const std::string&& name, u32 src, u32 dest, u64 pages):
    m_name(name), m_base_src(src), m_base_dest(dest),
    m_pages(pages), m_page_size(sysconf(_SC_PAGESIZE)), m_n_threads(1) {}

  virtual void setup() = 0;
  virtual std::vector<BenchmarkResults> run(u32 times) = 0;
  virtual void destroy() = 0;

  static constexpr bool verify() {return m_verify;}
  std::string name() const {return m_name;}

  template <typename func_t>
  inline u64 run_task_on_node(func_t task, const std::string &&task_name, u32 dest_node) {
    u64 elapsed;
    std::thread([&elapsed, dest_node, &task, task_name](){
      // Force to run this thread on the src numa node
      numa_run_on_node(dest_node);
      {
        Timer<std::chrono::nanoseconds> timer(task_name, elapsed);
        task();
      }
      // Remove binding, the rest can run whatever
      numa_run_on_node(-1);
    }).join();
    return elapsed;
  }

protected:
  u32 m_base_src;
  u32 m_base_dest;
  u64 m_pages;
  u64 m_page_size;
  u32 m_n_threads;

private:
  static constexpr bool m_verify {true};
  std::string m_name;
};
