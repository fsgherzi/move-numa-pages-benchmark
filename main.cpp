#include <iostream>
#include <vector>
#include <filesystem>
#include <omp.h>


#include "utils/fstypes.hpp"
#include "utils/popl.h"
#include "benchmarks/Benchmark.h"
#include "benchmarks/MovePagesBenchmark.h"
#include "benchmarks/MemcpyBenchmark.h"
#include "benchmarks/StreamCopyBenchmark.h"

using namespace fs;


i32 main(i32 argc, c8** argv) {

  popl::OptionParser options("memmove benchmark");

  auto pages_to_move_opt = options.add<popl::Value<u32>>("n", "pages", "number of pages to move");
  auto dest_numa_node_opt = options.add<popl::Value<u32>>("d", "destination", "Destination NUMA node");
  auto src_numa_node_opt = options.add<popl::Value<u32>>("s", "source", "Source NUMA node");
  auto repeat_times_opt = options.add<popl::Value<u32>>("t", "times", "how many times to repeat the benchmark");
  auto results_file_opt = options.add<popl::Value<std::string>>("o", "output", "path to the results file");
  auto benchmark_name_opt = options.add<popl::Value<std::string>>("b", "benchmark", "benchmark to run");
  auto n_threads_opt = options.add<popl::Value<u32>>("j", "jobs", "number of cores to use");

  options.parse(argc, argv);

  const auto results_path = results_file_opt->value();
  const auto benchmark_name = benchmark_name_opt->value();
  const auto pages_to_move = pages_to_move_opt->value();
  const auto dest_numa_node = dest_numa_node_opt->value();
  const auto src_numa_node =  src_numa_node_opt->value();
  const auto n_threads = n_threads_opt->value();

  omp_set_num_threads(n_threads);

  for(u32 thread_id = 0; thread_id < n_threads; ++thread_id) {

#pragma omp critical
    {
      std::cout << "Thread " << thread_id << " spawned.\n";
    }

  }

  Benchmark* benchmark {nullptr};
  std::string header = "id,name,src,dst,n_pages,page_size,time_ns,threads\n";

  if (std::filesystem::exists(results_path))
    header = "";

  std::ofstream results_file(results_path, std::ios::app);
  results_file << header;

//  if (numa_num_configured_nodes() == 1) {
//    std::cout << "[INFO] Have just one NUMA node. The benchmark cannot run." << std::endl;
//    exit(-1);
//  }

  if (benchmark_name == "move-pages")
    benchmark = new MovePagesBenchmark(src_numa_node, dest_numa_node, pages_to_move, n_threads);
  else if (benchmark_name == "memcpy")
    benchmark = new MemcpyBenchmark(src_numa_node, dest_numa_node, pages_to_move, n_threads);
  else if (benchmark_name == "stream-copy")
    benchmark = new StreamCopyBenchmark(src_numa_node, dest_numa_node, pages_to_move, n_threads);
  else
    return -1;

  benchmark->setup();

  auto results = benchmark->run(repeat_times_opt->value());
  for(u32 i = 0; i < results.size(); ++i)
    results_file << std::to_string(i) << "," << results[i] << ","<< n_threads <<"\n";

  benchmark->destroy();
  results_file.flush();

}