import os
import sys
from math import log2

if len(sys.argv) != 5:
    print("USAGE python runall.py <bin path> <out file path> <src_node> <dest_node> <max_threads>")
    os.exit(-1)

bin_path = sys.argv[1]
out_path = sys.argv[2]
src_node = sys.argv[3]
dest_node = sys.argv[4]
max_threads = int(sys.argv[5])

## get closest (rounded down) power of 2
max_threads_pwr = int(log2(max_threads))

n_pages = [2**i for i in range(8, 18)]
t = 10
benchmarks = ["stream-copy", "memcpy", "move-pages"]
for b in benchmarks:
    for n_p in n_pages:
        for tc in [2**j for j in range(0, max_threads_pwr)]:
            os.system(f"{bin_path} -n {n_p} -s {src_node} -d {dest_node} -t {t} -o {out_path} -b {b} -j{tc}")
