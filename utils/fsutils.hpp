//
// Created by lnghrdntcr on 8/3/22.
//
#pragma once

#include <chrono>
#include <string>
#include <iostream>
#include <type_traits>
#include <utility>

using sys_clock = std::chrono::system_clock;


namespace fs {

  template<typename time_granularity = std::chrono::nanoseconds>
  class Timer {
  public:
    Timer(std::string _name, u64 &elapsed_time, bool _enabled = true) :
      m_name(std::move(_name)), m_enabled(_enabled), m_begin(sys_clock::now()), m_elapsed_time(elapsed_time) {}
    Timer(std::string _name, bool _enabled = true) :
      m_name(std::move(_name)), m_enabled(_enabled), m_begin(sys_clock::now()), m_elapsed_time(_blank_elapsed_time) {}

    ~Timer() {
      auto elapsed = std::chrono::duration_cast<time_granularity>(sys_clock::now() - m_begin).count();
      if (m_enabled) {
        std::string time_granularity_symbol;
        if (std::is_same<time_granularity, std::chrono::nanoseconds>::value)
          time_granularity_symbol = "ns";
        if (std::is_same<time_granularity, std::chrono::microseconds>::value)
          time_granularity_symbol = "us";
        if (std::is_same<time_granularity, std::chrono::milliseconds>::value)
          time_granularity_symbol = "ms";
        if (std::is_same<time_granularity, std::chrono::seconds>::value)
          time_granularity_symbol = "s";

        m_elapsed_time = elapsed;
        std::cout << m_name << " took " << elapsed << time_granularity_symbol << std::endl;
      }
    }

  private:
    std::string m_name;
    const bool m_enabled{true};
    const std::chrono::time_point<sys_clock> m_begin;
    u64 _blank_elapsed_time;
    u64 &m_elapsed_time;
  };


  template<typename FunctionType>
  unsigned time_it(FunctionType function, const std::string &name = "", const bool debug = false) {
    auto begin = sys_clock::now();
    function();
    auto end = sys_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count();
    if (debug) std::cout << name << " took " << elapsed << "us" << std::endl;
    return elapsed;
  }
}
