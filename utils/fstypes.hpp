//
// Created by lnghrdntcr on 8/1/21.
//
#pragma once
#include <cstdint>

namespace fs {
	using c8  = char;
  using u8  = uint8_t;
	using u32 = uint32_t;
	using u64 = uint64_t;
	using i32 = int32_t;
	using i64 = int64_t;
	using f32 = float;
	using f64 = double;
  constexpr u64 B  = 8;
  constexpr u64 KB = 1024 * 8;
  constexpr u64 MB = 1024 * KB;
  constexpr u64 GB = 1024 * MB;
}
